#### GLOBAL
variable "project_name" {
  type        = string
  description = "Name of the project used to tag resources"
  default     = "test"
}

variable "project_owner" {
  type        = string
  description = "Name of the owner of the project, used to tag resources"
  default     = "admin"
}

variable "aws_region" {
  type        = string
  description = "AWS region where resources will be created"
  default     = "eu-west-1"
}

variable "aws_profile" {
  type        = string
  description = "AWS profile to be used by Terraform to build the infrastructure"
  default     = "terraform"
}

#### VPC
variable "vpc_name" {
  type        = string
  description = "Name used to tag the resource"
  default     = "vpc"
}

variable "vpc_cidr_block" {
  type        = string
  description = "CIDR block for the VPC"
  default     = "10.1.0.0/16"
}

variable "vpc_enable_dns_hostname" {
  type        = bool
  description = "Activate dns hostname for the VPC [default=true]"
  default     = true
}

#### NETWORK GENERIC
variable "netbits" {
  type        = string
  description = "Used to calculate CIDR blocks of subnets"
  default     = "8"
}

variable "sn_name" {
  type        = string
  description = "Generic name used to tag the resource"
  default     = "subnet"
}

variable "rt_name" {
  type        = string
  description = "Generic name used to tag the resource"
  default     = "routetable"
}

#### PUBLIC SUBNET AND NETWORK
variable "enable_public_subnets" {
  type        = bool
  description = "Allow the creation of public subnets if true [default=true]"
  default     = true
}

variable "public_networks_prefix" {
  type        = string
  description = "Prefix used for public subnets CIDR blocks"
  default     = "1"
}

variable "igw_name" {
  type        = string
  description = "Name used to tag the resource"
  default     = "igw"
}

#### PUBLIC SUBNET AND NETWORK
variable "enable_private_subnets" {
  type        = bool
  description = "Allow the creation of private subnets if true [default=true]"
  default     = true
}

variable "private_networks_prefix" {
  type        = string
  description = "Prefix used for private subnets CIDR blocks"
  default     = "8"
}

variable "nat_eip_name" {
  type        = string
  description = "Name used to tag the resource"
  default     = "nat_eip"
}

variable "nat_gw_name" {
  type        = string
  description = "Name used to tag the resource"
  default     = "ngw"
}