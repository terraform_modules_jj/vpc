#### GLOBAL
provider "aws" {
  region = var.aws_region
  profile = var.aws_profile
}

//terraform {
//  backend "s3" {
//    bucket                  = ""
//    key                     = "vpc/tfstate"
//  }
//}

data "aws_availability_zones" "availability_zones" {
  state = "available"
}

#### VPC
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = var.vpc_enable_dns_hostname

  tags = {
    Name  = "${var.project_name}-${terraform.workspace}-${var.vpc_name}"
    Owner = var.project_owner
  }
}

#### PUBLIC SUBNET AND NETWORK
resource "aws_subnet" "sn_public" {
  count  = var.enable_public_subnets ? length(data.aws_availability_zones.availability_zones) : 0
  vpc_id = aws_vpc.vpc.id
  cidr_block = cidrsubnet(
    aws_vpc.vpc.cidr_block,
    var.netbits,
    count.index + var.public_networks_prefix)
  availability_zone       = element(data.aws_availability_zones.availability_zones.names, count.index)
  map_public_ip_on_launch = true

  tags = {
    Name  = "${var.project_name}-${terraform.workspace}-${var.sn_name}-public-${count.index + 1}"
    Owner = var.project_owner
  }
}

resource "aws_internet_gateway" "igw" {
  count  = var.enable_public_subnets ? 1 : 0
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name  = "${var.project_name}-${terraform.workspace}-${var.igw_name}"
    Owner = var.project_owner
  }
}

resource "aws_route_table" "routetable_public" {
  count  = var.enable_public_subnets ? 1 : 0
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name  = "${var.project_name}-${terraform.workspace}-${var.rt_name}-public"
    Owner = var.project_owner
  }
}

resource "aws_route" "route_public" {
  count                  = var.enable_public_subnets ? 1 : 0
  route_table_id         = aws_route_table.routetable_public[0].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw[0].id
}

resource "aws_route_table_association" "rt_association_public" {
  count          = var.enable_public_subnets ? 1 : 0
  route_table_id = aws_route_table.routetable_public[0].id
  subnet_id      = element(aws_subnet.sn_public.*.id, count.index)
}

#### PRIVATE SUBNET AND NETWORK
resource "aws_subnet" "sn_private" {
  count  = var.enable_private_subnets ? length(data.aws_availability_zones.availability_zones) : 0
  vpc_id = aws_vpc.vpc.id
  cidr_block = cidrsubnet(
    aws_vpc.vpc.cidr_block,
    var.netbits,
    count.index + var.private_networks_prefix)
  availability_zone       = element(data.aws_availability_zones.availability_zones.names, count.index)
  map_public_ip_on_launch = false

  tags = {
    Name  = "${var.project_name}-${terraform.workspace}-${var.sn_name}-private-${count.index + 1}"
    Owner = var.project_owner
  }
}

resource "aws_eip" "nat_gateway_eip" {
  count = var.enable_private_subnets ? 1 : 0
  vpc   = true

  tags = {
    Name  = "${var.project_name}-${terraform.workspace}-${var.nat_eip_name}"
    Owner = var.project_owner
  }
}

resource "aws_nat_gateway" "ngw" {
  count         = var.enable_private_subnets ? 1 : 0
  allocation_id = aws_eip.nat_gateway_eip[0].id
  subnet_id     = aws_subnet.sn_public[0].id

  tags = {
    Name  = "${var.project_name}-${terraform.workspace}-${var.nat_gw_name}"
    Owner = var.project_owner
  }
}

resource "aws_route_table" "routetable_private" {
  count  = var.enable_private_subnets ? 1 : 0
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name  = "${var.project_name}-${terraform.workspace}-${var.rt_name}-private"
    Owner = var.project_owner
  }
}

resource "aws_route" "route_private" {
  count                  = var.enable_private_subnets ? 1 : 0
  route_table_id         = aws_route_table.routetable_private[0].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.ngw[0].id
}

resource "aws_route_table_association" "rt_association_private" {
  count          = var.enable_private_subnets ? 1 : 0
  route_table_id = aws_route_table.routetable_private[0].id
  subnet_id      = element(aws_subnet.sn_private.*.id, count.index)
}