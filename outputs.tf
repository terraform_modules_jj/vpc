output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "subnet_public_ids" {
  value = aws_subnet.sn_public.*.id
}

output "subnet_private_ids" {
  value = aws_subnet.sn_private.*.id
}

output "internet_gateway_id" {
  value = aws_internet_gateway.igw.id
}

output "eip_public_ip" {
  value = aws_eip.nat_gateway_eip.public_ip
}

output "nat_gateway_id" {
  value = aws_nat_gateway.ngw.id
}

output "nat_gateway_public_ip" {
  value = aws_nat_gateway.ngw.public_ip
}